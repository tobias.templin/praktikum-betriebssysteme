//! This test spawns two task and runs them.

#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(edu_kernel::test_runner)]

extern crate alloc;

#[no_mangle]
static FORCE_LINK: u8 = 0;

use alloc::vec::Vec;
use core::{
	ptr,
	sync::atomic::{AtomicUsize, Ordering},
};
use edu_kernel::scheduler::{self, TaskManager};

/// This function is run by the kernel after initialization.
#[no_mangle]
pub extern "C" fn _init() {
	static COUNT: AtomicUsize = AtomicUsize::new(0);

	// Each task atomically increments the variable COUNT
	extern "C" fn add(_: *mut ()) {
		COUNT.fetch_add(1, Ordering::Relaxed);
	}

	let tasks = (0..2)
		.map(|_| {
			scheduler::get().lock(|task_manager| task_manager.spawn_kernel(add, ptr::null_mut()))
		})
		.collect::<Vec<_>>();

	for task in tasks {
		TaskManager::join(scheduler::get(), task)
	}

	assert_eq!(2, COUNT.load(Ordering::Relaxed))
}
