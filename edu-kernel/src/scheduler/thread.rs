//! Native threads.
//!
//! Inspired from [`std::thread`](https://doc.rust-lang.org/std/thread/index.html).

use crate::scheduler::{self, Priority, TaskRef};
use alloc::{boxed::Box, sync::Arc};
use core::cell::UnsafeCell;
use scheduler::TaskManager;

/// This type is used to transmit the return value of a thread to the calling
/// function
type Packet<T> = Arc<UnsafeCell<Option<T>>>;

pub struct Handle<T> {
	task: TaskRef,
	packet: Packet<T>,
}

impl<T> Handle<T> {
	pub fn join(self) -> T {
		TaskManager::join(scheduler::get(), self.task);
		unsafe { (*self.packet.get()).take().unwrap() }
	}
}

// TODO: remove this unecessary struct indirection
/// An opaque thread type.
struct Thread;

impl Thread {
	fn spawn_prio(f: Box<dyn FnOnce() + 'static>, prio: Priority) -> TaskRef {
		// Put `f` onto the heap to allow passing it to `run_thread`.
		let p = Box::into_raw(Box::new(f));

		extern "C" fn run_thread(p: *mut ()) {
			unsafe {
				// Repack `f` into a `Box` to ensure proper destruction then run it.
				Box::from_raw(p as *mut Box<dyn FnOnce()>)();
			}
		}

		scheduler::get()
			.lock(|task_manager| task_manager.spawn_kernel_prio(run_thread, p as *mut _, prio))
	}
}

/// Spawns a new thread.
pub fn spawn<T: Send + 'static>(f: impl FnOnce() -> T + Send + 'static) -> Handle<T> {
	spawn_prio(f, Priority::NORMAL)
}

pub fn spawn_prio<T: Send + 'static>(
	f: impl FnOnce() -> T + Send + 'static,
	prio: Priority,
) -> Handle<T> {
	let packet: Packet<T> = Arc::new(UnsafeCell::new(None));
	let other_packet = packet.clone();

	let main = move || {
		let result = f();
		unsafe { *other_packet.get() = Some(result) };
	};

	let task = Thread::spawn_prio(Box::new(main), prio);

	Handle { task, packet }
}
