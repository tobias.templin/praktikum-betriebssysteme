// Copyright (c) 2018 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! Generic memory management functionality

pub mod bump_allocator;
pub mod freelist;

use crate::arch;
use crate::arch::mm::raw_mem_alloc::RAW_ALLOCATOR;
use alloc::alloc::Layout;
use core::{alloc::GlobalAlloc, fmt};
use log::info;

/// The error type for Memory Operations
#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Error {
	/// Called an Allocator which is not valid anymore.
	InvalidAllocatorCalled,
	OutOfMemory,
}

impl Error {
	fn as_str(&self) -> &'static str {
		match *self {
			Self::InvalidAllocatorCalled => {
				"Tried to allocate from which is not valid in this context"
			}
			Self::OutOfMemory => "No free memory available on this allocator",
		}
	}
}

impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f, "{}", self.as_str())
	}
}

/// Userspace allocator.
///
/// This allocator calls the allocation functions in libstd.
struct UserspaceAllocator;

unsafe impl GlobalAlloc for UserspaceAllocator {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		#[linkage = "weak"]
		#[no_mangle]
		unsafe fn libstd_alloc(_layout: Layout) -> *mut u8 {
			unimplemented!()
		}

		libstd_alloc(layout)
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		#[linkage = "weak"]
		#[no_mangle]
		unsafe fn libstd_dealloc(_ptr: *mut u8, _layout: Layout) {
			unimplemented!()
		}

		libstd_dealloc(ptr, layout)
	}
}

/// The global allocator.
///
/// Similar to the global panic handler, this allocator calls either the kernel
/// allocator or the userspace allocator depending on whether we are in
/// kernelspace or userspace respectively.
struct Allocator;

impl Allocator {
	/// Returns a reference to the allocator to use in the current
	/// kernel-/userspace.
	fn alloc() -> &'static dyn GlobalAlloc {
		if arch::in_kernelspace() {
			&freelist::FreelistAllocator
		} else {
			&UserspaceAllocator
		}
	}
}

unsafe impl GlobalAlloc for Allocator {
	unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
		Self::alloc().alloc(layout)
	}

	unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
		Self::alloc().dealloc(ptr, layout)
	}
}

/// This is the allocator object instance that Rust uses for things like `Vec`,
/// `Box`...
#[global_allocator]
static ALLOCATOR: Allocator = Allocator;


/// Initializes memory system
pub fn init() {
	info!("Initializing memory management");

	unsafe { arch::mm::init() };
	let memory = RAW_ALLOCATOR
		.lock(|a| unsafe { a.allocate_region(a.remaining_memory().unwrap()).unwrap() });
	freelist::FREELIST_MEMORY.lock(move |fl_mem| *fl_mem = Some(freelist::FreeList::new(memory)));
	RAW_ALLOCATOR.lock(|a| a.invalidate());
}


#[alloc_error_handler]
fn alloc_error_handler(layout: Layout) -> ! {
	panic!("[OOM] Allocation of {:?} bytes failed", layout)
}
