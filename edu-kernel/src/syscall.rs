//! System call routines.

use crate::scheduler::{self, TaskManager};
use alloc::alloc::{alloc, dealloc, Layout};
use core::{slice, str};

/// Table for syscall handlers.
///
/// This table has the addresses of the system call handlers.
#[repr(C)]
pub struct SyscallHandlerTable {
	handle: [*const (); 4],
}

impl SyscallHandlerTable {
	pub const fn new() -> Self {
		SyscallHandlerTable {
			handle: [
				exit_handler as _,
				alloc_handler as _,
				dealloc_handler as _,
				print_handler as _,
			],
		}
	}
}

/// # SAFETY
///
/// The offending field is `*const ()`, which is effectively `fn()`, which is
/// [`Sync`].
unsafe impl Sync for SyscallHandlerTable {}

/// Global system call handler table.
///
/// Used by the system call handler to determine the correct routine to execute.
pub static SYSCALL_HANDLER_TABLE: SyscallHandlerTable = SyscallHandlerTable::new();

/// Exit system call (0)
///
/// This system call terminates the current task. If the first argument is `0`,
/// the task is terminated with the success exit code.
extern "C" fn exit_handler(success: usize) -> ! {
	let status = if success == 0 { Ok(()) } else { Err(()) };
	TaskManager::exit(scheduler::get(), status)
}

/// Alloc system call (1)
///
/// This system call allocates memory with the specified `size` and `alignment`.
extern "C" fn alloc_handler(size: usize, align: usize) -> *mut u8 {
	let layout = Layout::from_size_align(size, align).unwrap();
	assert_ne!(0, layout.size(), "Cannot allocate memory with size 0.");
	// Safety: layout has non-zero size
	//todo!("implement alloc system call");
	let ptr = unsafe{alloc(layout)};
	return ptr;
}

/// Dealloc system call (2)
///
/// This system call deallocates memory.
extern "C" fn dealloc_handler(ptr: *mut u8, size: usize, align: usize) {
	// Safety: must be upheld by the allocator
	let layout = Layout::from_size_align(size, align).unwrap();
	unsafe{dealloc(ptr, layout)};
}

/// Print system call (3)
///
/// This system call prints the provided UTF-8 string to the standard output.
extern "C" fn print_handler(ptr: *const u8, len: usize) {
	// Safety: must be upheld by caller
	let slice = unsafe { slice::from_raw_parts(ptr, len) };
	let s = str::from_utf8(slice).unwrap();
	print!("{}",s);
	//warum Umweg über String? UTF-8 Format?
}
