//! A logger that prints to the terminal.
//!
//! Metadata is highlighted in color.

use crate::console::Color;
use core::fmt;
use log::{Level, LevelFilter, Metadata, Record, SetLoggerError};

/// Initializes the global logger
pub fn init() -> Result<(), SetLoggerError> {
	static COLOR_LOGGER: ColorLogger = ColorLogger;

	log::set_logger(&COLOR_LOGGER).map(|()| log::set_max_level(ColorLogger::LEVEL_FILTER))
}

/// A logger which displays verbosity level and kernel scopes in color.
struct ColorLogger;

impl ColorLogger {
	/// The level filter used by `ColorLogger`
	const LEVEL_FILTER: LevelFilter = LevelFilter::Debug;

	/// Determines whether the `record` comes from inside `edu_kernel`.
	fn is_kernel(record: &Record<'_>) -> bool {
		record.target().starts_with("edu_kernel")
	}
}

impl log::Log for ColorLogger {
	fn enabled(&self, metadata: &Metadata<'_>) -> bool {
		metadata.level() <= Self::LEVEL_FILTER
	}

	fn log(&self, record: &Record<'_>) {
		if self.enabled(record.metadata()) {
			if Self::is_kernel(record) {
				println!(
					"{reset}{color}[KERNEL]{reset} {level}: {args}",
					reset = Color::Reset,
					color = Color::CyanFG,
					level = ColoredLevel(record.level()),
					args = record.args(),
				)
			} else {
				println!("{}: {}", ColoredLevel(record.level()), record.args(),)
			}
		}
	}

	fn flush(&self) {}
}

/// A logger verbosity level which is being displayed in color.
struct ColoredLevel(Level);

impl ColoredLevel {
	/// Returns the [`Color`], this Level is displayed in.
	fn color(&self) -> Color {
		match self.0 {
			Level::Error => Color::RedFG,
			Level::Warn => Color::YellowFG,
			Level::Info => Color::Reset,
			Level::Debug => Color::GreenFG,
			Level::Trace => Color::BlueFG,
		}
	}
}

impl fmt::Display for ColoredLevel {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{reset}{color}{level}{reset}",
			reset = Color::Reset,
			color = self.color(),
			level = self.0
		)
	}
}
