// Copyright (c) 2017-2018 Stefan Lankes, RWTH Aachen University
//
// Licensed under the Apache License, Version 2.0, <LICENSE-APACHE or
// http://apache.org/licenses/LICENSE-2.0> or the MIT license <LICENSE-MIT or
// http://opensource.org/licenses/MIT>, at your option. This file may not be
// copied, modified, or distributed except according to those terms.

//! CPU context

use super::{gdt, stack::Stack};
use crate::scheduler::{self, TaskManager};
use core::{mem, ptr};
use x86_64::registers::{control::Cr0Flags, rflags::RFlags};

/// A CPU context than can be switched to.
///
/// This is used to store the registers of a preemted task. As this is pushed to
/// an x86 stack, which grows downwards, the last field is saved first and
/// restored last.
#[repr(C, packed)]
pub struct CpuContext {
	/// The GS segment register.
	gs: u64,
	/// The FS segment register.
	fs: u64,
	/// The RBP register (base pointer).
	rbp: u64,
	/// The RSP register (stack pointer).
	///
	/// This register is being skipped in [`CpuContext`] switches. It has
	/// already been set correctly to save/restore all other register to/from
	/// the correct stack.
	rsp: u64,
	/// The R15 general purpose register.
	r15: u64,
	/// The R14 general purpose register.
	r14: u64,
	/// The R13 general purpose register.
	r13: u64,
	/// The R12 general purpose register.
	r12: u64,
	/// The R11 general purpose register.
	r11: u64,
	/// The R10 general purpose register.
	r10: u64,
	/// The R9 general purpose register.
	r9: u64,
	/// The R8 general purpose register.
	r8: u64,
	/// The RDI general purpose register.
	rdi: u64,
	/// The RSI general purpose register.
	rsi: u64,
	/// The RDX general purpose register.
	rdx: u64,
	/// The RCX general purpose register.
	rcx: u64,
	/// The RBX general purpose register.
	rbx: u64,
	/// The RAX general purpose register.
	rax: u64,
	/// The RFLAGS status register.
	rflags: u64,
	/// The RIP register (instruction pointer).
	rip: u64,
}

impl CpuContext {
	/// Creates a CPU context.
	///
	/// # Safety
	///
	/// Runner must be an `extern "C" fn` never returns and which takes f and
	/// arg as arguments.
	unsafe fn new(stack: &Stack, runner: u64, f: u64, arg: u64) -> Self {
		let rflags = RFlags::IOPL_LOW;

		Self {
			gs: stack.top() as u64,
			fs: 0,
			rbp: stack.top() as u64,
			rsp: (stack.top() - mem::size_of::<Self>()) as u64,
			r15: 0,
			r14: 0,
			r13: 0,
			r12: 0,
			r11: 0,
			r10: 0,
			r9: 0,
			r8: 0,
			rdi: f,
			rsi: arg,
			rdx: 0,
			rcx: 0,
			rbx: 0,
			rax: 0,
			rflags: rflags.bits(),
			rip: runner,
		}
	}

	/// Creates a kernelspace CPU context.
	///
	/// This creates a stack frame for running `f` with `arg` on `stack`.
	fn new_kernel(stack: &Stack, f: extern "C" fn(*mut ()), arg: *mut ()) -> Self {
		/// Runs `f` with `arg` and never returns.
		///
		/// We actually start this function to ensure execution stops after the
		/// supplied function.
		extern "C" fn run_task(f: extern "C" fn(*mut ()), arg: *mut ()) -> ! {
			unsafe { scheduler::resume_preemption() }
			f(arg);
			TaskManager::exit(scheduler::get(), Ok(()))
		}

		unsafe {
			Self::new(
				stack,
				run_task as usize as u64,
				f as usize as u64,
				arg as usize as u64,
			)
		}
	}

	/// Creates a userspace CPU context.
	///
	/// This creates a stack frame for running `f` with `arg` on `stack`. As we
	/// cannot return to the kernel after `f`, it must not return. Either run
	/// indefinitely or exit via a system call.
	fn new_user(stack: &Stack, f: extern "C" fn(*mut ()) -> !, arg: *mut ()) -> Self {
		/// Runs `f` with `arg` in userspace.
		extern "C" fn run_task(f: extern "C" fn(*mut ()) -> !, arg: *mut ()) -> ! {
			unsafe { scheduler::resume_preemption() }
			// Jump to userspace:
			// We prepare an interrupt stack frame as if we were in userspace before and
			// then “return” to it at the end, jumping to `f` with `arg` in RDI as per C
			// calling convention.
			unsafe {
				asm!(
					"swapgs",
					"mov rbx, {user_data_segment_selector}",
					"mov ds, rbx",
					"mov es, rbx",
					"push rbx",
					"mov rbx, rsp",
					"push rbx",
					"pushfq",
					"mov rbx, {user_code_segment_selector}",
					"push rbx",
					"push {f}",
					"iretq",

					user_data_segment_selector = const gdt::USER_DATA_SEGMENT_SELECTOR.0,
					user_code_segment_selector = const gdt::USER_CODE_SEGMENT_SELECTOR.0,
					f = in(reg) f,
					in("rdi") arg,
					options(noreturn),
				)
			}
		}

		unsafe {
			Self::new(
				stack,
				run_task as usize as u64,
				f as usize as u64,
				arg as usize as u64,
			)
		}
	}

	/// Writes the CPU context to its stack.
	fn init(self) -> *mut Self {
		let stack_ptr = self.rsp as *mut Self;
		unsafe { ptr::write(stack_ptr, self) }
		stack_ptr
	}

	/// Initializes `stack` to run `f` with `arg` in kernelspace.
	///
	/// This creates a stack frame for the function at the top of the stack and
	/// returns the stack pointer to the initialized CPU context
	pub(crate) fn init_kernel(
		stack: &mut Stack,
		f: extern "C" fn(*mut ()),
		arg: *mut (),
	) -> *mut Self {
		Self::new_kernel(stack, f, arg).init()
	}

	/// Initializes `stack` to run `f` with `arg` in userspace.
	pub(crate) fn init_user(
		stack: &mut Stack,
		f: extern "C" fn(*mut ()) -> !,
		arg: *mut (),
	) -> *mut Self {
		Self::new_user(stack, f, arg).init()
	}

	/// Performs a [`CpuContext`] switch.
	///
	/// This pushes the current [`CpuContext`] to the current stack and saves
	/// the stack pointer in `_dst`. Then the stack pointer is set to `_src`
	/// where another [`CpuContext`] is popped from. The return address is
	/// switched as well by switching the stack. Thus this function returns to
	/// the `_src` [`CpuContext`] and might not return to its original
	/// [`CpuContext`].
	#[inline(never)]
	#[naked]
	pub extern "C" fn switch(_dst: *mut *mut CpuContext, _src: *mut CpuContext) {
		// extern "C":
		// `_dst` in rdi
		// `_src` in rsi

		unsafe {
			asm!(
				// Store context
				"pushfq",
				"push rax",
				"push rbx",
				"push rcx",
				"push rdx",
				"push rsi",
				"push rdi",
				"push r8",
				"push r9",
				"push r10",
				"push r11",
				"push r12",
				"push r13",
				"push r14",
				"push r15",
				"sub rsp, 8", // Skip storing RSP
				"push rbp",
				"rdfsbase rax",
				"push rax",
				"rdgsbase rbx",
				"push rbx",
				// Store current stack pointer with saved context in `_dst`.
				"mov [rdi], rsp",
				// Set stack pointer to supplied `_src`.
				"mov rsp, rsi",
				// Set task-switched flag in the CR0 register.
				"mov rax, cr0",
				"or rax, {task_switched}",
				"mov cr0, rax",
				// Set stack pointer in TSS.
				"call {set_current_kernel_stack}",
				// Restore context
				"pop rax",
				"wrgsbase rax",
				"pop rbx",
				"wrfsbase rbx",
				"pop rbp",
				"add rsp, 8", // Skip restoring RSP
				"pop r15",
				"pop r14",
				"pop r13",
				"pop r12",
				"pop r11",
				"pop r10",
				"pop r9",
				"pop r8",
				"pop rdi",
				"pop rsi",
				"pop rdx",
				"pop rcx",
				"pop rbx",
				"pop rax",
				"popfq",
				"ret",
				task_switched = const Cr0Flags::TASK_SWITCHED.bits(),
				set_current_kernel_stack = sym gdt::set_current_kernel_stack,
				options(noreturn)
			)
		}
	}
}
