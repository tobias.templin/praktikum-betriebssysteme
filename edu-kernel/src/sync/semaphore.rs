//! A simple Semaphore Implementation based on a
//! [InterruptSpinlock](crate::sync::interrupt::InterruptSpinlock)
//!
//! A semaphore is a basic signaling mechanism. It can be used to manage access to limited number of resources or for basic communication. A classic use case is the [Produces-Consumer problem](https://en.wikipedia.org/wiki/Producer%E2%80%93consumer_problem)

use crate::scheduler::{self, fifo_scheduler::FifoScheduler, Scheduler, TaskManager};
use crate::sync::interrupt::InterruptSpinlock;

/// A simple counting semaphore implementation.
///
/// # Example usage:
///
/// ```
/// let sem: Arc<Semaphore> = Arc::new(Semaphore::new(0));
/// let mut threads = Vec::new();
///
/// threads.push({
///     let sem = sem.clone();
///     thread::spawn(|| sem.wait())
/// });
/// threads.push({
///     let sem = sem.clone();
///     thread::spawn(|| sem.wait())
/// });
/// for t in threads.iter().for_each(|t| t.join())
/// ```
pub struct Semaphore {
	/// The counting variable
	mutex: InterruptSpinlock<usize>,
	/// A list of waiting tasks
	waiting: InterruptSpinlock<Option<FifoScheduler>>,
}
impl Semaphore {
	/// Create a new semaphore with an initial count of `count`.
	pub const fn new(count: usize) -> Self {
		Self {
			mutex: InterruptSpinlock::new(count),
			waiting: InterruptSpinlock::new(None),
		}
	}

	/// Request access to a resource protected by this semaphore.
	///
	/// If the thread has to wait for the resource, it will be blocked and
	/// another thread gets scheduled. This is the counterpart to
	/// [`Semaphore::post()`].
	pub fn wait(&self) {
		let mut block = false;
		self.mutex.lock(|c| {
			if *c == 0 {
				// waiting
				self.waiting.lock(|queue| {
					queue
						.get_or_insert_with(Default::default)
						.insert(scheduler::get().lock(TaskManager::block_current_task))
				});

				block = true;
			} else {
				*c -= 1;
			}
		});
		if block {
			// Switch to another task
			TaskManager::yield_now(scheduler::get())
		}
	}

	/// Grant access to the protected resource.
	///
	/// One of the waiting tasks gets scheduled, if any. This is the counterpart
	/// to [`Semaphore::wait()`]
	pub fn post(&self) {
		self.mutex.lock(|c| {
			// Wakeup one waiting task if available
			if let Some(task) = self.waiting.lock(|queue| {
				if let Some(iter) = queue.as_mut() {
					iter.next()
				} else {
					None
				}
			}) {
				scheduler::get().lock(|task_manager| task_manager.wakeup_task(task))
			} else {
				*c += 1;
			}
		});
	}
}
// Check if Semaphore can be made `Send` correctly. For now, lets just assume it
// does... :-D
unsafe impl Send for Semaphore {}
unsafe impl Sync for Semaphore {}
