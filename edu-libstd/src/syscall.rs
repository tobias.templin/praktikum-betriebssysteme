//! System calls

/// An enumeration of eduOS' system calls.
///
/// A variants integer value is the corresponding 0th argument in system calls.
pub enum Syscall {
	Exit,
	Alloc,
	Dealloc,
	Print,
}
