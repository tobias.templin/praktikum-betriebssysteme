# eduOS - A teaching operating system written in Rust

eduOS is a Unix-like operating system based on a monolithic architecture for educational purposes.
It is developed for the course [Operating Systems][acsos] at RWTH Aachen University and includes a modified hypervisor that simplifies the boot process to increase the intelligibility of the OS.
eduOS is inspired from following tutorials and software distributions:

1. Philipp Oppermann's [excellent series of blog posts][opp].
2. Erik Kidd's [toyos-rs][kidd], which is an extension of Philipp Opermann's kernel.
3. The original version of [eduOS][stlankes], which was the old teaching kernel written in C.

[opp]: https://blog.phil-opp.com/
[kidd]: http://www.randomhacks.net/bare-metal-rust/
[stlankes]: https://rwth-os.github.io/eduOS/
[rust-barebones-kernel]: https://github.com/thepowersgang/rust-barebones-kernel
[acsos]: https://www.acs.eonerc.rwth-aachen.de/cms/E-ON-ERC-ACS/Studium/Lehrveranstaltungen/~gbso/Betriebssysteme/


## Structure

- [`edu-kernel`](edu-kernel): This is the Kernel itself. It contains the bootcode and all the kernel subsystems.
- [`edu-application`](edu-application): This is an example application for the kerne.
- [`edu-libstd`](edu-libstd): Provides the glue between the application and the kernel.
- [`edu-testmacro`](edu-testmacro): Simple implementation of the `#[test]` macro for a barebones test framework.

## Requirements to build eduOS

eduOS only runs on [ehyve](https://github.com/RWTH-OS/ehyve). Take a look at ehyve's GitHub page for installation instructions.

eduOS also requires a nightly Rust compiler.

## Building

eduOS can be build and run using Cargo:

Running edu-application:
```bash
> cargo run
```

Running all tests:
```bash
> cargo test
```

Running tests for one of the subsystems:
```bash
> cargo test --package edu-application

# alternatively: run cargo test in one of the subprojects:

> cd edu-kernel
> cargo test
```

## License

Licensed under either of

 * Apache License, Version 2.0, ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.
